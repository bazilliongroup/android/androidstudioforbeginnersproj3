package com.example.androidstudioforbeginnersproj3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ItemAdapter extends BaseAdapter {

    LayoutInflater mInflater;
    String[] items;
    String[] movies;
    String[] descriptions;

    public ItemAdapter(Context c, String[] i, String[] m, String[] d){

        items = i;
        movies = m;
        descriptions = d;
        mInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int i) {
        return items[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = mInflater.inflate(R.layout.my_listview_detail1, null);
        TextView nameTextView = (TextView) v.findViewById(R.id.nameTextView);
        TextView descriptionTextView = (TextView) v.findViewById(R.id.decriptiontextView);
        TextView movieTextView = (TextView) v.findViewById(R.id.movieTextView);

        String name = items[i];
        String desc = descriptions[i];
        String mov = movies[i];

        nameTextView.setText(name);
        descriptionTextView.setText(desc);
        movieTextView.setText(mov);

        return v;
    }
}
